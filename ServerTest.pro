TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp

HEADERS += \
    Client.h

win32 {
    LIBS += "C:\\Users\\zamf\\Desktop\\ServerTest\\ViconDataStreamSDK_CPP.lib"
}

OTHER_FILES += \
    ViconDataStreamSDK_CPP.dll \
    boost_thread-vc90-mt-1_43.dll



